/* A polyfill for browsers that don't support ligatures. */
/* The script tag referring to this file must be placed before the ending body tag. */

/* To provide support for elements dynamically added, this script adds
   method 'icomoonLiga' to the window object. You can pass element references to this method.
*/
(function () {
    'use strict';
    function supportsProperty(p) {
        var prefixes = ['Webkit', 'Moz', 'O', 'ms'],
            i,
            div = document.createElement('div'),
            ret = p in div.style;
        if (!ret) {
            p = p.charAt(0).toUpperCase() + p.substr(1);
            for (i = 0; i < prefixes.length; i += 1) {
                ret = prefixes[i] + p in div.style;
                if (ret) {
                    break;
                }
            }
        }
        return ret;
    }
    var icons;
    if (!supportsProperty('fontFeatureSettings')) {
        icons = {
            'home': '&#xe90d;',
            'house': '&#xe90d;',
            'home2': '&#xe90f;',
            'house2': '&#xe90f;',
            'home3': '&#xe910;',
            'house3': '&#xe910;',
            'image': '&#xe911;',
            'picture': '&#xe911;',
            'copy': '&#xe92c;',
            'duplicate': '&#xe92c;',
            'paste': '&#xe92d;',
            'clipboard-file': '&#xe92d;',
            'stack': '&#xe92e;',
            'layers': '&#xe92e;',
            'location': '&#xe947;',
            'map-marker': '&#xe947;',
            'location2': '&#xe948;',
            'map-marker2': '&#xe948;',
            'compass': '&#xe949;',
            'direction': '&#xe949;',
            'compass2': '&#xe94a;',
            'direction2': '&#xe94a;',
            'map': '&#xe94b;',
            'guide': '&#xe94b;',
            'map2': '&#xe94c;',
            'guide2': '&#xe94c;',
            'history': '&#xe94d;',
            'time': '&#xe94d;',
            'display': '&#xe956;',
            'screen': '&#xe956;',
            'laptop': '&#xe957;',
            'computer': '&#xe957;',
            'mobile': '&#xe958;',
            'cell-phone': '&#xe958;',
            'mobile2': '&#xe959;',
            'cell-phone2': '&#xe959;',
            'floppy-disk': '&#xe962;',
            'save2': '&#xe962;',
            'drive': '&#xe963;',
            'save3': '&#xe963;',
            'database': '&#xe964;',
            'db': '&#xe964;',
            'user': '&#xe971;',
            'profile2': '&#xe971;',
            'users': '&#xe972;',
            'group': '&#xe972;',
            'user-plus': '&#xe973;',
            'user2': '&#xe973;',
            'user-minus': '&#xe974;',
            'user3': '&#xe974;',
            'user-check': '&#xe975;',
            'user4': '&#xe975;',
            'user-tie': '&#xe976;',
            'user5': '&#xe976;',
            'search': '&#xe986;',
            'magnifier': '&#xe986;',
            'zoom-in': '&#xe987;',
            'magnifier2': '&#xe987;',
            'zoom-out': '&#xe988;',
            'magnifier3': '&#xe988;',
            'enlarge': '&#xe989;',
            'expand': '&#xe989;',
            'shrink': '&#xe98a;',
            'collapse': '&#xe98a;',
            'wrench': '&#xe991;',
            'tool': '&#xe991;',
            'equalizer': '&#xe992;',
            'sliders': '&#xe992;',
            'equalizer2': '&#xe993;',
            'sliders2': '&#xe993;',
            'cog': '&#xe994;',
            'gear': '&#xe994;',
            'cogs': '&#xe995;',
            'gears': '&#xe995;',
            'pie-chart': '&#xe99a;',
            'stats': '&#xe99a;',
            'stats-dots': '&#xe99b;',
            'stats2': '&#xe99b;',
            'stats-bars': '&#xe99c;',
            'stats3': '&#xe99c;',
            'stats-bars2': '&#xe99d;',
            'stats4': '&#xe99d;',
            'leaf': '&#xe9a4;',
            'nature': '&#xe9a4;',
            'meter': '&#xe9a6;',
            'gauge': '&#xe9a6;',
            'meter2': '&#xe9a7;',
            'gauge2': '&#xe9a7;',
            'truck': '&#xe9b0;',
            'transit': '&#xe9b0;',
            'road': '&#xe9b1;',
            'asphalt': '&#xe9b1;',
            'target': '&#xe9b3;',
            'goal': '&#xe9b3;',
            'list-numbered': '&#xe9b9;',
            'options': '&#xe9b9;',
            'list': '&#xe9ba;',
            'todo': '&#xe9ba;',
            'list2': '&#xe9bb;',
            'todo2': '&#xe9bb;',
            'sphere': '&#xe9c9;',
            'globe': '&#xe9c9;',
            'earth': '&#xe9ca;',
            'globe2': '&#xe9ca;',
            'eye': '&#xe9ce;',
            'views': '&#xe9ce;',
            'eye-plus': '&#xe9cf;',
            'views2': '&#xe9cf;',
            'eye-minus': '&#xe9d0;',
            'views3': '&#xe9d0;',
            'eye-blocked': '&#xe9d1;',
            'views4': '&#xe9d1;',
            'loop2': '&#xea2e;',
            'repeat2': '&#xea2e;',
            'file-pdf': '&#xeadf;',
            'file10': '&#xeadf;',
          '0': 0
        };
        delete icons['0'];
        window.icomoonLiga = function (els) {
            var classes,
                el,
                i,
                innerHTML,
                key;
            els = els || document.getElementsByTagName('*');
            if (!els.length) {
                els = [els];
            }
            for (i = 0; ; i += 1) {
                el = els[i];
                if (!el) {
                    break;
                }
                classes = el.className;
                if (/sf/.test(classes)) {
                    innerHTML = el.innerHTML;
                    if (innerHTML && innerHTML.length > 1) {
                        for (key in icons) {
                            if (icons.hasOwnProperty(key)) {
                                innerHTML = innerHTML.replace(new RegExp(key, 'g'), icons[key]);
                            }
                        }
                        el.innerHTML = innerHTML;
                    }
                }
            }
        };
        window.icomoonLiga();
    }
}());
